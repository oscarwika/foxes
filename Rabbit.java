
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * A simple model of a rabbit. Rabbits age, move, breed, and die.
 *
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public class Rabbit extends Agent {
    // Characteristics shared by all rabbits (class variables).

    // The age at which a rabbit can start to breed.
    private static final int BREEDING_AGE = 120; //5
    // The age to which a rabbit can live.
    private static final int MAX_AGE = 365; //40
    // The likelihood of a rabbit breeding.
    private static final double BREEDING_PROBABILITY = 0.020;    //Default 0.08
    // The maximum number of births.
    private static final int MAX_LITTER_SIZE = 8;   //default 4, avg. 6 min 1 to max 14
    // A shared random number generator to control breeding.
    private static final Random rand = Randomizer.getRandom();
    // Chance of rabbit born with disease
    private static final double DISEASE_PROBABILITY = 0.02; // Default 0.01

    // Individual characteristics (instance fields).
    // The rabbit's age.
    private int age;

    /**
     * Create a new rabbit. A rabbit may be created with age zero (a new born)
     * or with a random age.
     *
     * @param randomAge If true, the rabbit will have a random age.
     * @param field The field currently occupied.
     * @param location The location within the field.
     */
    public Rabbit(boolean randomAge, Field field, Location location) {
        super(field, location);
        age = 0;
        if (randomAge) {
            age = rand.nextInt(MAX_AGE);
        }
    }

    /**
     * This is what the rabbit does most of the time - it runs around. Sometimes
     * it will breed or die of old age.
     *
     * @param newRabbits A list to return newly born rabbits.
     */
    public void act(List<Agent> newRabbits) {
        if (isAlive()) {
            incrementAge();
            if (isAlive()) {
                giveBirth(newRabbits);
                // Try to move into a free location.
                Location newLocation = moveAround();
                if (isAlive()) {
                    if (newLocation == null) {
                        newLocation = getField().freeAdjacentLocation(getLocation());
                    }
                    if (newLocation != null) {
                        setLocation(newLocation);
                    } else {
                        // Overcrowding.
                        setDead(CauseOfDeath.OVERPOPULATION);
                    }
                }
            }
        }
    }

    public int getAge() {
        return this.age;
    }

    /**
     * The rabbit tries to move to a free space, could result in a killed by
     * snare.
     *
     * @return
     */
    private Location moveAround() {
        Field field = getField();
        List<Location> adjacent = field.adjacentLocations(getLocation());
        Iterator<Location> it = adjacent.iterator();
        while (it.hasNext()) {
            Location where = it.next();
            Object agent = field.getObjectAt(where);
            if (agent instanceof Snare) {
                Snare snare = (Snare) agent;
                //setDeathCause(CauseOfDeath.SNARE);
                this.setDead(deathCause.SNARE);
                snare.setDead(deathCause.USED);
            }
        }
        return null;
    }

    /**
     * Increase the age. This could result in the rabbit's death.
     */
    protected void incrementAge() {
        age++;
        if (age > MAX_AGE) {
            //setDeathCause(CauseOfDeath.AGE);
            setDead(CauseOfDeath.AGE);
        }
    }

    /**
     * Check whether or not this rabbit is to give birth at this step. New
     * births will be made into free adjacent locations.
     *
     * @param newRabbits A list to return newly born rabbits.
     */
    private void giveBirth(List<Agent> newRabbits) {
        // New rabbits are born into adjacent locations.
        // Get a list of adjacent free locations.
        Field field = getField();
        List<Location> free = field.getFreeAdjacentLocations(getLocation());
        int births = breed();
        for (int b = 0; b < births && free.size() > 0; b++) {
            if (Math.random() > DISEASE_PROBABILITY) {
                Location loc = free.remove(0);
                Rabbit young = new Rabbit(false, field, loc);
                newRabbits.add(young);
            } else {
                Location loc = free.remove(0);
                DiseasedRabbit young = new DiseasedRabbit(false, field, loc);
                newRabbits.add(young);
            }

        }
    }

    /**
     * Generate a number representing the number of births, if it can breed.
     *
     * @return The number of births (may be zero).
     */
    protected int breed() {
        int births = 0;
        if (canBreed() && rand.nextDouble() <= BREEDING_PROBABILITY) {
            births = rand.nextInt(MAX_LITTER_SIZE) + 1;
        }
        return births;
    }

    /**
     * A rabbit can breed if it has reached the breeding age.
     *
     * @return true if the rabbit can breed, false otherwise.
     */
    private boolean canBreed() {
        return age >= BREEDING_AGE;
    }
}
