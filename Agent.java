
import java.util.List;

/**
 * A class representing shared characteristics of animals.
 *
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public abstract class Agent {

    // Whether the animal is alive or not.
    private boolean alive;
    // The animal's field.
    private Field field;
    // The animal's position in the field.
    private Location location;

    protected CauseOfDeath deathCause;

    /**
     * Create a new animal at location in field.
     *
     * @param field The field currently occupied.
     * @param location The location within the field.
     */
    public Agent(Field field, Location location) {
        alive = true;
        this.field = field;
        setLocation(location);
    }

    /**
     * Make this animal act - that is: make it do whatever it wants/needs to do.
     *
     * @param newAnimals A list to receive newly born animals.
     */
    abstract public void act(List<Agent> newAnimals);

    public CauseOfDeath getDeathCause() {
        return deathCause;
    }

    public void setDeathCause(CauseOfDeath deathCause) {
        this.deathCause = deathCause;
    }

    /**
     * Check whether the animal is alive or not.
     *
     * @return true if the animal is still alive.
     */
    protected boolean isAlive() {
        return alive;
    }

    /**
     * Indicate that the animal is no longer alive. It is removed from the
     * field.
     */
    protected void setDead(CauseOfDeath causeOfDeath) {
        
        if (this instanceof Fox) {
            Fox f = (Fox) this;
            Simulator.foxDeathLogger.write(Simulator.step + "," + Simulator.getFoxPopulation() + "," + f.getAge() + "," + causeOfDeath.getCauseValue() + "," + f.getLocation()); //+location
        }
        if (this instanceof Rabbit && this != null) {
            Rabbit r = (Rabbit) this;
            Simulator.rabbitDeathLogger.write(Simulator.step + "," + Simulator.getRabbitPopulation() + "," + r.getAge() + "," + causeOfDeath.getCauseValue() + "," + r.getLocation()); //+location
        }
        alive = false;
        if (location != null) {
            field.clear(location);
            location = null;
            field = null;
        }
    }

    /**
     * Return the animal's location.
     *
     * @return The animal's location.
     */
    protected Location getLocation() {
        return location;
    }

    /**
     * Place the animal at the new location in the given field.
     *
     * @param newLocation The animal's new location.
     */
    protected void setLocation(Location newLocation) {
        if (location != null) {
            field.clear(location);
        }
        location = newLocation;
        field.place(this, newLocation);
    }

    /**
     * Return the animal's field.
     *
     * @return The animal's field.
     */
    protected Field getField() {
        return field;
    }
}
