
import java.util.Random;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A simple predator-prey simulator, based on a rectangular field containing
 * rabbits and foxes.
 *
 * @author David J. Barnes and Michael K�lling
 * @version 2011.07.31
 */
public class Simulator {

    // Constants representing configuration information for the simulation.
    // The default width for the grid.
    private static final int DEFAULT_WIDTH = 200;
    // The default depth of the grid.
    private static final int DEFAULT_DEPTH = 150;
    // The probability that a fox will be created in any given grid position.
    private static final double FOX_CREATION_PROBABILITY = 0.006; // Default 0.015
    // The probability that a rabbit will be created in any given grid position.
    private static final double RABBIT_CREATION_PROBABILITY = 0.15; // Default 0.08
    //Probability of hunter
    private static final double HUNTER_CREATION_PROBABILITY = 0.002;
    // Probability of diseased rabbit
    private static final double DISEASEDRABBIT_CREATION_PROBABILITY = 0.005;
    private static final double SNARE_CREATION_PROBABILITY = 0.01;
    private static ArrayList<SimulatorWorker> threadList = new ArrayList<>();
    private ReentrantLock simLock = new ReentrantLock();
    public static PrintLog foxDeathLogger = new PrintLog("FoxDeaths");
    public static PrintLog rabbitDeathLogger = new PrintLog("RabbitDeaths");
    public static PrintLog popLogger = new PrintLog("PopLogger");
    public static PrintLog deathLogger = new PrintLog("DeathLogger");

    // List of animals in the field.
    private List<Agent> animals;
    private ArrayList<Agent> deathList;
    // The current state of the field.
    private Field field;
    // The current step of the simulation.
    public static int step; //private int
    private static int foxPop;
    private static int rabbitPop;
    // A graphical view of the simulation.
    private SimulatorView view;

    /**
     * Construct a simulation field with default size.
     */
    public Simulator() {
        this(DEFAULT_DEPTH, DEFAULT_WIDTH);
    }

    /**
     * Create a simulation field with the given size.
     *
     * @param depth Depth of the field. Must be greater than zero.
     * @param width Width of the field. Must be greater than zero.
     */
    public Simulator(int depth, int width) {
        if (width <= 0 || depth <= 0) {
            System.out.println("The dimensions must be greater than zero.");
            System.out.println("Using default values.");
            depth = DEFAULT_DEPTH;
            width = DEFAULT_WIDTH;
        }

        animals = new ArrayList<Agent>();
        deathList = new ArrayList<>();
        field = new Field(depth, width);

        // Create a view of the state of each location in the field.
        view = new SimulatorView(depth, width, this);
        view.setColor(Rabbit.class, Color.ORANGE);
        view.setColor(Fox.class, Color.BLUE);
        view.setColor(Hunter.class, Color.GREEN);
        view.setColor(Snare.class, Color.MAGENTA);
        view.setColor(DiseasedRabbit.class, Color.RED);

        // Setup a valid starting point.
        reset();
    }

    /**
     * Run the simulation from its current state for a reasonably long period,
     * (X steps).
     */
    public void runLongSimulation() throws FileNotFoundException, InterruptedException {
        String deathHeader = "Step,Population,Age,DeathCause,Loc_Row,Loc_Col,Age=0,OP=1,Starv=2,Snare=3";
        foxDeathLogger.write(deathHeader + ",Infect=5");
        rabbitDeathLogger.write(deathHeader + ",Eaten=4");

        String popHeader = "Step,RabbitPop,SnarePop,FoxPop,DiseasedRabbitPop,HunterPop,R_Died,F_Died";
        /*String popHeader = "Step,RabbitPop,SnarePop,FoxPop,DiseasedRabbitPop,HunterPop,R_Died,R_DiedAge,R_DiedOC,R_DeathEaten, R_DiedSnare,"
                + "F_Died,F_DiedAge,F_DiedOC,F_DiedInfection,F_DiedStarved, F_DiedSnare";*/
        String deathLogHeader = "Step,R_D_TOTAL,R_D_Age,R_D_DISEASE,R_D_OC,R_D_EATEN,R_D_SNARE,"
                + "F_D_TOTAL,F_D_AGE,F_D_OC,F_D_INFECTION,F_D_STARVED,F_D_SNARE";
        popLogger.write(popHeader);
        deathLogger.write(deathLogHeader);

        simulate(60000);
        /*foxDeathLogger.writeToFile();
        rabbitDeathLogger.writeToFile();
        popLogger.writeToFile();
        deathLogger.writeToFile();*/
    }

    /**
     * Run the simulation from its current state for the given number of steps.
     * Stop before the given number of steps if it ceases to be viable.
     *
     * @param numSteps The number of steps to run for.
     */
    public void simulate(int numSteps) throws FileNotFoundException, InterruptedException {
        SimulatorWorker worker = new SimulatorWorker(numSteps);
        threadList.add(worker);
        worker.start();
        worker.join();

        foxDeathLogger.writeToFile();
        rabbitDeathLogger.writeToFile();
        popLogger.writeToFile();
        deathLogger.writeToFile();
    }

    /**
     * Run the simulation from its current state for a single step. Iterate over
     * the whole field updating the state of each fox and rabbit.
     */
    public void simulateOneStep() {
        step++;

        // Provide space for newborn animals.
        List<Agent> newAnimals = new ArrayList<Agent>();
        deathList = new ArrayList();
        // Let all rabbits act.
        for (Iterator<Agent> it = animals.iterator(); it.hasNext();) {
            Agent animal = it.next();
            animal.act(newAnimals);
            if (!animal.isAlive()) {
                it.remove();
                deathList.add(animal);
            }
        }

        // Add the newly born foxes and rabbits to the main lists.
        animals.addAll(newAnimals);

        view.showStatus(step, field);
        printStatistics();
    }

    private void printStatistics() {
        int rabbitPop = 0;
        int snarePop = 0;
        int foxPop = 0;
        int diseasedRabbitPop = 0;
        int hunterPop = 0;

        for (Iterator<Agent> it = animals.iterator(); it.hasNext();) {
            Agent animal = it.next();
            if (animal instanceof Rabbit) {
                rabbitPop++;
            }
            if (animal instanceof Snare) {
                snarePop++;
            }
            if (animal instanceof Fox) {
                foxPop++;
            }
            if (animal instanceof DiseasedRabbit) {
                diseasedRabbitPop++;
            }
            if (animal instanceof Hunter) {
                hunterPop++;
            }
        }

        int rabbitDeathCount = 0;
        int rabbitOldAge = 0;
        int rabbitOvercrowding = 0;
        int rabbitEaten = 0;
        int rabbitSnared = 0;

        int rabbitDisease = 0;
        //TODO statistics off diseased rabbits

        int foxDeathCount = 0;
        int foxOldAge = 0;
        int foxOvercrowding = 0;
        int foxInfected = 0;
        int foxStarved = 0;
        int foxSnared = 0;

        for (Iterator<Agent> it = deathList.iterator(); it.hasNext();) {
            Agent animal = it.next();
            if (animal instanceof Rabbit) {
                rabbitDeathCount++;
                Rabbit rab = (Rabbit) animal;
                if (rab.getDeathCause() == CauseOfDeath.AGE) {
                    rabbitOldAge++;
                }
                if (rab.getDeathCause() == CauseOfDeath.OVERPOPULATION) {
                    rabbitOvercrowding++;
                }
                if (rab.getDeathCause() == CauseOfDeath.EATEN) {
                    rabbitEaten++;
                }
                if (rab.getDeathCause() == CauseOfDeath.SNARE) {
                    rabbitSnared++;
                }
            }

            if (animal instanceof Fox) {
                foxDeathCount++;
                Fox fox = (Fox) animal;
                if (fox.getDeathCause() == CauseOfDeath.AGE) {
                    foxOldAge++;
                }
                if (fox.getDeathCause() == CauseOfDeath.OVERPOPULATION) {
                    foxOvercrowding++;
                }
                if (fox.getDeathCause() == CauseOfDeath.INFECTED) {
                    foxInfected++;
                }
                if (fox.getDeathCause() == CauseOfDeath.STARVATION) {
                    foxStarved++;
                }
                if (fox.getDeathCause() == CauseOfDeath.SNARE) {
                    foxSnared++;
                }
            }
        }
        setRabbitPopulation(rabbitPop);
        setFoxPopulation(foxPop);
        popLogger.write(step + "," + rabbitPop + "," + snarePop + "," + foxPop + "," + diseasedRabbitPop + "," + hunterPop + ","
                + rabbitDeathCount + "," + foxDeathCount + ",");
        deathLogger.write(step + "," + rabbitDeathCount + "," + rabbitOldAge
                + "," + rabbitDisease + "," + rabbitOvercrowding + "," + rabbitEaten + "," + rabbitSnared + "," + foxDeathCount
                + "," + foxOldAge + "," + foxOvercrowding + "," + foxInfected + "," + foxStarved + "," + foxSnared + "," + foxPop + "," + rabbitPop);
    }

    /**
     * Reset the simulation to a starting position.
     */
    public void reset() {
        step = 0;
        animals.clear();
        populate();

        // Show the starting state in the view.
        view.showStatus(step, field);
    }

    public static int getFoxPopulation() {
        return foxPop;
    }
    
    public void setFoxPopulation(int foxP) {
        this.foxPop = foxP;
    }

    public static int getRabbitPopulation() {
        return rabbitPop;
    }
    
    public void setRabbitPopulation(int rabbitP) {
        this.rabbitPop = rabbitP;
    }
    
    /**
     * Randomly populate the field with foxes and rabbits.
     */
    private void populate() {
        Random rand = Randomizer.getRandom();
        field.clear();
        for (int row = 0; row < field.getDepth(); row++) {
            for (int col = 0; col < field.getWidth(); col++) {
                if (rand.nextDouble() <= FOX_CREATION_PROBABILITY) {
                    Location location = new Location(row, col);
                    Fox fox = new Fox(true, field, location);
                    animals.add(fox);
                } else if (rand.nextDouble() <= RABBIT_CREATION_PROBABILITY) {
                    Location location = new Location(row, col);
                    Rabbit rabbit = new Rabbit(true, field, location);
                    animals.add(rabbit);
                } else if (rand.nextDouble() <= HUNTER_CREATION_PROBABILITY) {
                    Location location = new Location(row, col);
                    Hunter hunter = new Hunter(true, field, location);
                    animals.add(hunter);
                } else if (rand.nextDouble() <= DISEASEDRABBIT_CREATION_PROBABILITY) {
                    Location location = new Location(row, col);
                    DiseasedRabbit aidsRabbit = new DiseasedRabbit(true, field, location);
                    animals.add(aidsRabbit);
                } else if (rand.nextDouble() <= SNARE_CREATION_PROBABILITY) {
                    Location location = new Location(row, col);
                    Snare snare = new Snare(field, location);
                    animals.add(snare);
                }
                // else leave the location empty.
            }
        }
    }

    public class SimulatorWorker extends Thread {

        private int numSteps = 1;

        public SimulatorWorker(int i) {
            this.numSteps = i;

        }

        @Override
        public void run() {

            try {
                simLock.lockInterruptibly();
                for (int step = 1; step <= numSteps && view.isViable(field); step++) {
                    simulateOneStep();
                    //printStatistics();
                }
            } catch (InterruptedException ex) {
                System.out.println("wtf? Why you spam me....?");
            } finally {
                if (simLock.isHeldByCurrentThread()) {
                    simLock.unlock();
                    threadList.remove(this);
                }
            }
        }
    }

}
