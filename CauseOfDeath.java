
/**
 *
 * @author Kjetil
 */
public enum CauseOfDeath {
    AGE(0), OVERPOPULATION(1), STARVATION(2), SNARE(3), EATEN(4), INFECTED(5), DISEASED(6), USED(7);

    private int causeValue;

    CauseOfDeath(int causeValue) {
        this.causeValue = causeValue;
    }

    public int getCauseValue() {
        return causeValue;
    }
}
