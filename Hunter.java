
import java.util.List;
import java.util.Random;

/**
 *
 * @author Kjetil Yndestad
 * @version Feb 13, 2017
 */
class Hunter extends Agent {

    private int age;
    private static final Random rand = Randomizer.getRandom();
    private static final double trapProb = 0.1;

    public Hunter(boolean randomAge, Field field, Location location) {
        super(field, location);
        age = 0;
    }

    @Override
    public void act(List<Agent> newAnimals) {
        Location newLocation = getField().freeAdjacentLocation(getLocation());
        if (newLocation != null) {
            Location oldLoc = getLocation();
            setLocation(newLocation);
            if (rand.nextDouble() < trapProb) {
                setTrapLocation(newAnimals, oldLoc);
            }

        }
        //walk + trap with probability
        //release dogsa
        //increment age
    }

    private void setTrapLocation(List<Agent> newSnare, Location oldLoc) {
        Field field = getField();
        Snare snare = new Snare(field, oldLoc);
        newSnare.add(snare);
    }

}
