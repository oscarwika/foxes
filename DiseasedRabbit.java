
import java.util.List;


/**
 *
 * @author Oscar Wika
 */
public class DiseasedRabbit extends Rabbit {
    
    
    private int counter = 0;
    
    private String deathCause;
    
    public DiseasedRabbit(boolean randomAge, Field field, Location location) {
        super(randomAge, field, location);
    }
    
    @Override
    protected void incrementAge() {
        super.incrementAge();
        counter++;
        if (counter >= 10) {
            if(Math.random() < 0.7) {
                setDead(CauseOfDeath.DISEASED);
            }
            counter = 0;
        }
       
    }

    @Override
    public void act(List<Agent> newDiseasedRabbits) {
        incrementAge();
        if (isAlive()) {
            giveBirth(newDiseasedRabbits);
            // Try to move into a free location.
            Location newLocation = getField().freeAdjacentLocation(getLocation());
            if (newLocation != null) {
                setLocation(newLocation);
            } else {
                // Overcrowding.
                //System.out.println("Rabbit-died: " + age + " cause(Overcrowding)");
                //deathCause = "Overcrowding";
                setDead(CauseOfDeath.OVERPOPULATION);

            }
        }
    }

    /**
     * 
     * @param newDiseasedRabbits 
     */
    protected void giveBirth(List<Agent> newDiseasedRabbits) {
        // New rabbits are born into adjacent locations.
        // Get a list of adjacent free locations.
        Field field = getField();
        List<Location> free = field.getFreeAdjacentLocations(getLocation());
        int births = super.breed();
        for (int b = 0; b < births && free.size() > 0; b++) {
            Location loc = free.remove(0);
            DiseasedRabbit young = new DiseasedRabbit(false, field, loc);
            newDiseasedRabbits.add(young);
        }
    }
    
    
    
    
    
}
