
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Kjetil Yndestad
 * @version Feb 27, 2017
 */
public class PrintLog {

    private String name;
    private ArrayList<String> msgList;
    private File file;

    public PrintLog(String name) {
        this.name = name;
        msgList = new ArrayList<String>();
        file = new File(name + ".csv");
    }

    public void write(String msg) {
        msgList.add(msg);
    }

    public void writeToFile() {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(name+".csv", false));            
            for (String s : msgList) {           
                bw.write(s + "\n");
            }
            bw.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Something fucky fileNotFound" + name);
        } catch (IOException ex) {
            System.out.println("Something wrong ioException" + name);
        }
        
    }

}
