
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author Oscar Wika
 */
public class Main {
    public static final Simulator SIMULATOR = new Simulator();
    /**
     *
     * @param args
     */
    public static void main(String[] args) throws FileNotFoundException {
        try {
            SIMULATOR.runLongSimulation();
        } catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
